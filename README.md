# Netflix_Wrapped

## Project Title
### Real-time Personalized Content Analytics and Recommendation System [Netflix Wrapped]

## Problem Statement
The aim of this project is to create a website that shows the stats of the user's Netflix watch-history in a given time period. We focus to enhance user engagement and satisfaction by delivering personalized content recommendations and insights through a scalable and efficient platform.

## Team Members
1. Apoorva Yadav
2. Abhaya Trivedi
3. Khushi Kashyap
4. Akshita Sure
5. Mitiksha Paliwal

## Work Flow
1. User Authentication 
2. Data Extraction from Account 
3. Data Preprocessing  
4. Frontend  
5. Movie Recommendation System
6. How is bad is your Movie/Show taste?
7. Netflix Blend
8. Deployment
9. Polishing

## Presentation Link
https://prezi.com/view/oKKLcCIXoZDFn4oiFhy7/
