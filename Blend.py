import pandas as pd

def load_and_process_viewing_history(user_num):
    # Prompt the user to enter the file path for the Netflix viewing history CSV
    file_path = input(f"Enter the file path for user {user_num}'s Netflix viewing history CSV: ")
    
    # Load the Netflix viewing history CSV
    df = pd.read_csv(file_path)
    df['Title'] = df['Title'].astype(str)
    df['Date'] = pd.to_datetime(df['Date'], format='%d/%m/%y', errors='coerce')
    df = df[df['Date'].dt.year == 2024]
    df[f'show_title{user_num}'] = [s.partition(':')[0] for s in df['Title']]
    
    return df

def merge_with_main_titles(df, user_num):
    # Load the main Netflix titles CSV
    df_main = pd.read_csv("//wsl.localhost/Ubuntu/home/khushikashyap/netflix_wrapped/netflix_titles.csv")
    
    # Merge user viewing history with main titles
    df_mg = pd.merge(df, df_main, left_on=f'show_title{user_num}', right_on='title', how='inner')
    
    # Separate movies and TV shows
    movies_df = df_mg[df_mg['type'] == 'Movie'].drop_duplicates(subset=f'show_title{user_num}')
    shows_df = df_mg[df_mg['type'] == 'TV Show']
    
    return movies_df, shows_df

# Call the functions for the first user
df1 = load_and_process_viewing_history(1)
movies_df1, shows_df1 = merge_with_main_titles(df1, 1)

# Call the functions for the second user
df2 = load_and_process_viewing_history(2)
movies_df2, shows_df2 = merge_with_main_titles(df2, 2)

#Calculates common movie for both users
movies_count1 = movies_df1['show_title1'].value_counts()
movies_count2 = movies_df2['show_title2'].value_counts()

#Finds common movies
common_movies = movies_count1.index.intersection(movies_count2.index)
common_movies_counts = (movies_count1[common_movies] + movies_count2[common_movies]).sort_values(ascending=False)

#calculates show count
shows_count1 = shows_df1['show_title1'].value_counts()
shows_count2 = shows_df2['show_title2'].value_counts()

#Calculates common shows 
common_shows = shows_count1.index.intersection(shows_count2.index)
common_shows_counts = (shows_count1[common_shows] + shows_count2[common_shows]).sort_values(ascending=False)

combined_count1 = shows_count1.add(movies_count1, fill_value=0)
combined_count2 = shows_count2.add(movies_count2, fill_value=0)

common_titles = combined_count1.index.intersection(combined_count2.index)
common_titles_counts = (combined_count1[common_titles] + combined_count2[common_titles]).sort_values(ascending=False)

#Calculates similarity percentage according to their list of common movie and show watched
total_unique_titles = len(combined_count1.index.union(combined_count2.index))
common_titles_count = len(common_titles)

similarity_percentage = (common_titles_count / total_unique_titles) * 100

#Prints a particular tagline for different percentage
def get_similarity_tag(similarity):
    if 90 <= similarity <= 100:
        return "You should be best friends!"
    elif 70 <= similarity < 90:
        return "You have a lot in common!"
    elif 50 <= similarity < 70:
        return "There are some common grounds!"
    elif 30 <= similarity < 50:
        return "Don't worry, they say opposites attract!"
    elif 10 <= similarity < 30:
        return "You have quite different tastes!"
    else:
        return "It might be tough finding common ground!"

tag = get_similarity_tag(similarity_percentage)

print(f"Similarity in taste percentage: {similarity_percentage:.2f}%")
print(f"Tag: {tag}")

# Printing their common show watched
if not common_shows_counts.empty:
    most_watched_common_show = common_shows_counts.idxmax()
    most_watched_common_count = common_shows_counts.max()
    print(f"You both love {most_watched_common_show}! It's a binge-worthy match!")
else:
    print("It looks like you don't have a common show yet. Maybe it's time to plan a binge-watching session together!")

#Printing their common movie watched
if not common_movies_counts.empty:
    top_common_movie = common_movies_counts.idxmax()
    print(f" You both enjoyed {top_common_movie} together.")
else:
    print("Looks like you don't have a common movie yet. Maybe it's time to plan a movie night together ")

# Function to recommend random unwatched movies
def recommend_random_unwatched_movies(df_main1, df_main2, df_mg1, df_mg2):
    # Get all unique movies from df_main1 and df_main2
    all_movies = pd.concat([df_main1['title'], df_main2['title']]).unique()

    # Get movies watched by users
    watched_movies_user1 = df_mg1['title'].unique()
    watched_movies_user2 = df_mg2['title'].unique()

    # Filter unwatched movies
    unwatched_movies = []
    for movie in all_movies:
        if movie not in watched_movies_user1 and movie not in watched_movies_user2:
            unwatched_movies.append(movie)

    # Select random unwatched movies
    np.random.shuffle(unwatched_movies)
    recommended_movies = unwatched_movies[:5]  # Recommend 5 random movies

    return recommended_movies

# calling the function to print recommendation for movies 
recommended_random_movies = recommend_random_unwatched_movies(df_main1, df_main2, df_mg1, df_mg2)
print("Here are some hidden gems you haven't watched yet. Grab the popcorn and enjoy!:")
for i, movie in enumerate(recommended_random_movies, 1):
    print(f"{i}. {movie}")



# Function to recommend random unwatched shows
def recommend_random_unwatched_shows(df_main1, df_main2, df_mg1, df_mg2):
    # Get all unique shows from df_main1 and df_main2
    all_shows = pd.concat([df_main1[df_main1['type'] == 'TV Show']['title'], df_main2[df_main2['type'] == 'TV Show']['title']]).unique()

    # Get shows watched by users
    watched_shows_user1 = df_mg1[df_mg1['type'] == 'TV Show']['title'].unique()
    watched_shows_user2 = df_mg2[df_mg2['type'] == 'TV Show']['title'].unique()

    # Filter unwatched shows
    unwatched_shows = []
    for show in all_shows:
        if show not in watched_shows_user1 and show not in watched_shows_user2:
            unwatched_shows.append(show)

    # Select random unwatched shows
    np.random.shuffle(unwatched_shows)
    recommended_shows = unwatched_shows[:5]  # Recommend 5 random shows

    return recommended_shows

#Calling function to print recommendation for shows
recommended_random_shows = recommend_random_unwatched_shows(df_main1, df_main2, df_mg1, df_mg2)
print("Discover these binge-worthy series you both haven't seen:")
for i, show in enumerate(recommended_random_shows, 1):
    print(f"{i}. {show}")

